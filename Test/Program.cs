﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Parser;

namespace Test {
	class Program {
		static void Main(string[] args) {
			HTTPSender http = new HTTPSender("www.naver.com");
			http.SetHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) TestAgent");
			http.SetHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			http.SetHeader("Accept-Language", "ko-KR,ko;q=0.8,en-US;q=0.5,en;q=0.3");
			http.SetHeader("Accept-Encoding", "none");
			http.SetHeader("Connection", "close");
			http.SetHeader("Pragma", "no-cache");
			http.SetHeader("Cache-Control", "no-cache");
			http.OnSuccess += Http_OnSuccess;
			http.GET("/", false);
		}

		private static void Http_OnSuccess(object sender, HttpResponseEventArgs e) {
			string html = Encoding.UTF8.GetString(e.Response);
			Stopwatch timelog = new Stopwatch();
			timelog.Start();
			HtmlDomObject dom = new HtmlDomObject(html);
			Console.WriteLine("new HtmlDomObject(string) - " + timelog.ElapsedMilliseconds + "ms 경과");
			timelog.Reset();
			string output = dom.find("dt#rankTitle")[0].html();
			Console.WriteLine("HtmlDomNode::find(\"dt#rankTitle\") - " + timelog.ElapsedMilliseconds + "ms 경과");
			timelog.Reset();
			HtmlDomNode realrank = dom.find("ol#realrank")[0];
			Console.WriteLine("HtmlDomNode::find(\"ol#realrank\") - " + timelog.ElapsedMilliseconds + "ms 경과");
			timelog.Reset();
			int i = 1;
			foreach(HtmlDomNode li in realrank.children) {
				string content = li.children[0].html();
				output += "\n" + i + ". " + content.Substring(0, content.IndexOf('<'));
				++i;
			}
			timelog.Stop();
			Console.WriteLine(output);
		}
	}
}
