﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;

namespace Parser {
	public class HttpResponseEventArgs : EventArgs {
		public bool Success = false;
		public Exception FailException;

		public string method;
		public string Host;
		public int Port = 80;
		public string URI;
		public Dictionary<string, string> REQ_Header = new Dictionary<string, string>();
		public string REQ_Body;

		public int HTTP_Code;
		public string HTTP_Message;
		public Dictionary<string, string> Header = new Dictionary<string, string>();
		public byte[] Response;

		public object tag;

		public HttpResponseEventArgs(Exception FailException) {
			this.Success = false;
			this.FailException = FailException;
		}

		public HttpResponseEventArgs(byte[] req, string header, byte[] res) {
			Regex header_regex = new Regex(@"([^\r\n: ]+): ?([^\r\n]+)");
			this.Success = true;
			string[] tmp = header.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
			string[] tmp2 = tmp[0].Split(new char[] { ' ' });
			this.HTTP_Code = int.Parse(tmp2[1]);
			this.HTTP_Message = tmp2[2];
			this.Response = res;

			for(int i = 1; i < tmp.Length; i++) {
				Match match = header_regex.Match(tmp[i]);
				if(match.Success) {
					if(this.Header.ContainsKey(match.Groups[1].Value)) {
						if(match.Groups[1].Value == "Set-Cookie") {
							this.Header[match.Groups[1].Value] += "; " + match.Groups[2].Value;
						}
						else {
							this.Header[match.Groups[1].Value] = match.Groups[2].Value;
						}
					}
					else {
						this.Header.Add(match.Groups[1].Value, match.Groups[2].Value);
					}
				}
			}

			string req_string = Encoding.Default.GetString(req);
			tmp = req_string.Split(new string[] { "\r\n" }, StringSplitOptions.None);
			int offset = 0;
			tmp2 = tmp[0].Split(new char[] { ' ' });
			while(tmp2[0].Length == 0) {
				offset++;
				tmp2 = tmp[offset].Split(new char[] { ' ' });
			}
			this.method = tmp2[0];
			this.URI = tmp2[1];

			int j = offset + 1;
			while(j < tmp.Length - 2) {
				Match match = header_regex.Match(tmp[j]);
				if(match.Success) {
					if(this.REQ_Header.ContainsKey(match.Groups[1].Value)) {
						this.REQ_Header[match.Groups[1].Value] = match.Groups[2].Value;
					}
					else {
						this.REQ_Header.Add(match.Groups[1].Value, match.Groups[2].Value);
					}
				}
				j++;
			}
			this.REQ_Body = tmp[j + 1];

			//this.Host = this.REQ_Header["Host"];
        }
	}
}