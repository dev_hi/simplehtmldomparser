﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser {
	public class HasNoChildException : HtmlDomException {

		public const string DefaultMessage = "CSS 선택자 검색결과가 없습니다.";

		public HasNoChildException() : base() {}

		public HasNoChildException(string message) : base(message) {}

		public HasNoChildException(string message, int line) : base(message, line) { }

		public HasNoChildException(string message, string html) : base(message, html) { }

		public HasNoChildException(string message, string html, int line) : base(message, html, line) { }
	}
}
