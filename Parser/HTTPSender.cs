﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Web;

namespace Parser {
	public class HTTPSender {
		public string Host;
		public IPAddress Dest_IP;
		private int port = 80;
		public int Dest_Port {
			get {
				return this.port;
			}
			set {
				this.port = value;
				this.Dest_EndPoint = new IPEndPoint(this.Dest_IP, value);
			}
		}
		public IPEndPoint Dest_EndPoint;
		public bool KeepAlive = true;

		private Dictionary<string, string> Header = new Dictionary<string, string>();

		public delegate void ResponseReceiveHandler(object sender, HttpResponseEventArgs e);
		public event ResponseReceiveHandler OnSuccess = delegate { };
		public event ResponseReceiveHandler OnFail = delegate { };

		Socket TCP_SOCKET = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		Thread CommThread;
		Queue<WaitCallback> WorkQueue = new Queue<WaitCallback>();
		Queue<ParameterizedThreadStart> ThreadQueue = new Queue<ParameterizedThreadStart>();

		public string tag = "";

		public HTTPSender(string host) {
			//CommThread = ThreadPool.
			this.Host = host;
			IPAddress[] ip_addr_array = Dns.GetHostAddresses(host);
            if(ip_addr_array.Length > 0) {
				this.Dest_IP = ip_addr_array[0];
				this.Dest_EndPoint = new IPEndPoint(this.Dest_IP, this.port);
			}
			else {
				throw new Exception("도메인 주소를 확인할 수 없었습니다.");
			}
		}

		public void GET(string URI, bool no_warning) {
			this.Request("GET", URI, !no_warning);
		}

		public void POST(string URI, Dictionary<string, string> FormData, bool no_warning) {
			this.Request("POST", URI, !no_warning, FormData);
		}

		public void SetHeader(string key, string value) {
			if(!(new string[] { "Host", "Content-Length", "Content-Type", "Connection" }).Contains<string>(key)) {
				if(this.Header.ContainsKey(key)) {
					this.Header[key] = value;
				}
				else {
					this.Header.Add(key, value);
				}
			}
		}

		public void RemoveHeader(string key) {
			if(this.Header.ContainsKey(key)) {
				this.Header.Remove(key);
			}
		}

		public string GetHeaderString() {
			StringBuilder sb = new StringBuilder();
			foreach(KeyValuePair<string, string> h in this.Header) {
				sb.AppendFormat("{0}: {1}\r\n", h.Key, h.Value);
			}
			sb.AppendFormat("Connection: {0}", (this.KeepAlive ? "Keep-Alive" : "Close"));
			return sb.ToString();
		}

		public string MakeRequestString(string method, string uri, bool normal_packet) {
			StringBuilder sb = new StringBuilder();
			if(!normal_packet) sb.Append("\r\n");
			sb.AppendFormat("{0} {1} HTTP/1.1\r\n", method.ToUpper(), uri);
			if(normal_packet) sb.AppendFormat("Host: {0}\r\n", this.Host);
			sb.Append(this.GetHeaderString());
			if(!normal_packet) sb.AppendFormat("\r\nHost: {0}", this.Host);
			return sb.ToString();
		}

		public void Request(string method, string uri, bool normal_packet) {
			List<byte[]> ret = new List<byte[]>();
			string header = this.MakeRequestString(method, uri, normal_packet);
			string body = "\r\n\r\n";
			ret.Add(Encoding.Default.GetBytes(String.Concat(header, normal_packet ? body : "")));
			if(!normal_packet) ret.Add(Encoding.Default.GetBytes(body));
			this.Send(ret);
		}

		public void Request(string method, string uri, bool normal_packet, Dictionary<string, string> FormData) {
			List<byte[]> ret = new List<byte[]>();
			StringBuilder sb = new StringBuilder();
			foreach(KeyValuePair<string, string> data in FormData) {
				sb.AppendFormat("{0}={1}&", data.Key, HttpUtility.UrlEncode(data.Value));
			}
			string body = String.Concat("\r\n\r\n", sb.ToString().Substring(0, sb.Length - 1));
			this.Header.Add("Content-Type", "application/x-www-form-urlencoded");
			this.Header.Add("Content-Length", body.Length.ToString());
			string header = this.MakeRequestString(method, uri, normal_packet);
			this.RemoveHeader("Content-Type");
			this.RemoveHeader("Content-Length");
			ret.Add(Encoding.Default.GetBytes(String.Concat(header, normal_packet ? body : "")));
			if(!normal_packet)
				ret.Add(Encoding.Default.GetBytes(body));
			this.Send(ret);
		}

		public void Send(List<byte[]> buffer_list) {
			try {
				MemoryStream buffer_ms = new MemoryStream();
				if(!TCP_SOCKET.Connected) {
					TCP_SOCKET = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					TCP_SOCKET.Connect(this.Dest_EndPoint);
				}
				if(TCP_SOCKET.Connected) {
					foreach(byte[] buffer in buffer_list) {
						TCP_SOCKET.Send(buffer, SocketFlags.None);
						buffer_ms.Write(buffer, 0, buffer.Length);
					}

					bool headerReading = true;
					string header = "";
					int contentLength = 0;
					bool chunked = false;
					while(headerReading) {
						byte[] headBuffer = new byte[1];
						TCP_SOCKET.Receive(headBuffer, 0, 1, SocketFlags.None);
						header += Encoding.Default.GetString(headBuffer);
						if(header.Contains("\r\n\r\n")) {
							Regex reg = new Regex("\r\nContent-Length: (.*?)\r\n");
							Regex reg2 = new Regex("\r\nTransfer-Encoding: *chunked\r\n");
							Match matches = reg.Match(header);
							Match matches2 = reg2.Match(header);
							if(matches2.Success) {
								chunked = true;
							}
							else if(matches.Success) {
								contentLength = int.Parse(matches.Groups[1].ToString());
								//TCP_SOCKET.ReceiveBufferSize = contentLength;
							}
							headerReading = false;
						}
					}

					byte[] body;

					if(chunked) {
						bool keep = true;
						MemoryStream totalReceivedBodyBuffer = new MemoryStream();
						while(keep) {
							int chunk_length = 0;
							string chunk_length_hex = "";

							while(!chunk_length_hex.Contains("\r\n") && chunk_length_hex != "0\r\n") {
								byte[] chunkLengthBuffer = new byte[1];
								TCP_SOCKET.Receive(chunkLengthBuffer, 0, 1, SocketFlags.None);
								chunk_length_hex += Encoding.Default.GetString(chunkLengthBuffer);
							}

							chunk_length_hex = chunk_length_hex.Replace("\r\n", "");
							if(chunk_length_hex.Length != 0) {
								chunk_length = Convert.ToInt32(chunk_length_hex, 16);
								if(chunk_length == 0) {
									keep = false;
								}
								else {
									for(int i = 0; i < chunk_length; i++) {
										byte[] r_buffer = new byte[1];
										TCP_SOCKET.Receive(r_buffer, 0, 1, SocketFlags.None);
										totalReceivedBodyBuffer.Write(r_buffer, 0, 1);
									}

								}
							}
						}

						body = totalReceivedBodyBuffer.ToArray();
						totalReceivedBodyBuffer.Close();
					}
					else {
						MemoryStream totalReceivedBodyBuffer = new MemoryStream(contentLength);
						int now_received_length = 0;
						while(contentLength != totalReceivedBodyBuffer.Length) {
							byte[] now_receiving = new byte[TCP_SOCKET.ReceiveBufferSize];
							now_received_length = TCP_SOCKET.Receive(now_receiving, 0, now_receiving.Length, SocketFlags.None);
							if(Encoding.Default.GetString(now_receiving).Length > 0)
								totalReceivedBodyBuffer.Write(now_receiving, 0, now_received_length);
						}

						body = totalReceivedBodyBuffer.ToArray();
						totalReceivedBodyBuffer.Close();
					}

					bool ServerSocketClose = false;

					HttpResponseEventArgs httpevent = new HttpResponseEventArgs(buffer_ms.ToArray(), header, body);
					httpevent.Host = this.Host;
					if(this.port != 80) httpevent.Port = this.port;
					if(httpevent.Header.ContainsKey("Connection"))
						ServerSocketClose = httpevent.Header["Connection"].ToLower() == "close";

					if(ServerSocketClose)
						TCP_SOCKET.Close();

					this.OnSuccess(this, httpevent);
				}
				else {
					HttpResponseEventArgs httpevent = new HttpResponseEventArgs(new SocketException((int)SocketError.NotConnected));
					if(this.port != 80) httpevent.Port = this.port;
					this.OnFail(this, httpevent);
				}
				buffer_ms.Close();
			}
			catch(SocketException ex) {
				HttpResponseEventArgs httpevent = new HttpResponseEventArgs(ex);
				if(this.port != 80) httpevent.Port = this.port;
				if(this.OnFail != null) this.OnFail(this, httpevent);
			}
		}
	}
}
