﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Haron.Parser {

	public class HtmlDomObject {
		public string origHtml;
		public Dictionary<int,int> excluded = new Dictionary<int,int>();
		public string html;
		public int size;
		public HtmlDomNode document;
		public List<HtmlDomNode> NodeList = new List<HtmlDomNode>();

		List<string> blockTag = new List<string> { };

		HtmlDomNode current_parent;

		public HtmlDomObject(string html) {
			this.origHtml = html;
			
			this.html = html;
			this.size = html.Length;
			this.document = new HtmlDomNode(this, "document", DomNodeType.NODETYPE_ROOT, -1);
			this.current_parent = this.document;
			
			//this.remove_string(@"<!\[CDATA\[([\s\S]*?)\]\]>");
			//this.remove_string(@"<!--([\s\S]*?)-->");
			//this.remove_string(@"<\s*script[^>]*[^/]>([\s\S]*?)<\s*/\s*script\s*>");
			//this.remove_string(@"<\s*script\s*>([\s\S]*?)<\s*/\s*script\s*>");
			//this.remove_string(@"<\s*style[^>]*[^/]>([\s\S]*?)<\s*/\s*style\s*>");
			//this.remove_string(@"<\s*style\s*>([\s\S]*?)<\s*/\s*style\s*>");
			//this.remove_string(@"<\s*(?:code)[^>]*>([\s\S]*?)<\s*/\s*(?:code)\s*>");
			//this.remove_string(@"(<\?)([\s\S]*?)(\?>)");
			//this.remove_string(@"(\{\w)([\s\S]*?)(\})");
			
			Regex NodeExtractor = new Regex(@"<[^>]*>|[^<>]+");
			Regex tagParser = new Regex("\\w*=|\"[^\"]*\"|'[^']*'|\\w*\\w");
			MatchCollection matches = NodeExtractor.Matches(this.html);
			int depth = 0;
			int matchesCount = matches.Count;

			for (int i = 0; i < matchesCount; i++) {
				Match matching = matches[i];
				if ((matching.Value[0] == '<' && matching.Value[matching.Value.Length - 1] == '>')) {
					MatchCollection tagParsed = tagParser.Matches(matching.Value);
					string tagname = tagParsed[0].Value;
					if (matching.Value.Substring(0, matching.Value.IndexOf(tagname)).Contains("/") && !this.blockTag.Contains(tagname)) {
						this.blockTag.Add(tagname);
					}
				}
			}

			for(int j = 0; j < matchesCount; j++) {
				Match matching = matches[j];
				bool isTextNode = !(matching.Value[0] == '<' && matching.Value[matching.Value.Length - 1] == '>');
				int TagType = DomNodeType.NODETYPE_UNKNOWN;

				if (isTextNode) {
					TagType = DomNodeType.NODETYPE_TEXT;



					HtmlDomNode tmpNode = new HtmlDomNode(this, "", TagType, 0);
					tmpNode.pos[0] = matching.Index;
					tmpNode.pos[1] = matching.Index + matching.Length;
					tmpNode.text = matching.Value;

					if (depth < 0)
						throw new OutOfNodeDepthException(OutOfNodeDepthException.DefaultMessage, tmpNode.toString(), tmpNode.line);
						
					this.current_parent.append(tmpNode);
					tmpNode.nodeDepth = depth;

				} else {
					MatchCollection tagParsed = tagParser.Matches(matching.Value);
					string tagname = "";
					Dictionary<string, string> attrDic = new Dictionary<string, string>();
					int tagParsedCount = tagParsed.Count;

					for (int i = 0; i < tagParsedCount; i++) {
						string v = tagParsed[i].Value;
						if (i == 0)
							tagname = v;
						else if (v[v.Length - 1] == '=') {
							string key = v.Substring(0, v.Length - 1);
							string v2 = tagParsed[i + 1].Value;
							string value = "";
							if (v2[v2.Length - 1] == '=')
								value = "";
							else {
								value = v2.Substring(1, v2.Length - 2);
								i++;
							}

							if (attrDic.ContainsKey(key))
								attrDic[key] = value;
							else {
								attrDic.Add(key, value);
							}
						}
					}
					if (matching.Value.Substring(0, matching.Value.IndexOf(tagname)).Contains("/"))
						TagType = DomNodeType.NODETYPE_EOBT;
					else if (blockTag.Contains(tagname))
						TagType = DomNodeType.NODETYPE_BTAG;
					else
						TagType = DomNodeType.NODETYPE_STAG;
					int origIndex = matching.Index;

					foreach (KeyValuePair<int, int> pair in this.excluded) {
						if (pair.Key < matching.Index) {
							origIndex += pair.Value;
						}
					}

					HtmlDomNode tmpNode = new HtmlDomNode(this, tagname, TagType, this.count_char('\n', this.origHtml.Substring(0, origIndex)));
					tmpNode.pos[0] = matching.Index;
					tmpNode.pos[1] = matching.Index + matching.Length;
					tmpNode.tag = matching.Value;
					tmpNode.attributes = attrDic;

					if (depth < 0)
						throw new OutOfNodeDepthException(OutOfNodeDepthException.DefaultMessage, tmpNode.toString(), tmpNode.line);

					if (TagType == DomNodeType.NODETYPE_EOBT) {
						depth--;
						this.current_parent.text = this.html.Substring(this.current_parent.pos[0], tmpNode.pos[1] - this.current_parent.pos[0]);
						this.current_parent = this.current_parent.parent;
						//this.current_parent.append(tmpNode);
					} else if (TagType == DomNodeType.NODETYPE_BTAG) {
						depth++;
						this.current_parent.append(tmpNode);
						this.current_parent = tmpNode;
					} else {
						this.current_parent.append(tmpNode);
					}

					tmpNode.nodeDepth = depth;

					if (TagType != DomNodeType.NODETYPE_EOBT)
						this.NodeList.Add(tmpNode);

				}
			}
		}

		public HtmlDomNode[] find(string selector) {
			return this.document.find(selector);
		}

		public void remove_string(string pattern) {
			Regex finder = new Regex(pattern);
			MatchCollection stringTOremove = finder.Matches(this.html);
			int count = stringTOremove.Count;
			for(int i = 0; i < count; i++) {
				this.excluded.Add(stringTOremove[i].Index, stringTOremove[i].Length);
				this.html = this.html.Replace(stringTOremove[i].Value, "");
			}
		}

		public int count_char(char needle, string haystack) {
			int count = 0;
			int length = haystack.Length;
			for(int i = 0; i < length; i++) {
				if(haystack[i] == needle)
					count++;
			}
			return count;
		}

		public override string ToString() {
			string ret = "";
			foreach(HtmlDomNode n in document.children) {
				ret += n.ToString();
			}
			return ret ;
		}
	}
}
